#!/bin/bash
echo "deb http://security.kali.org/kali-security kali/updates main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://security.kali.org/kali-security kali/updates main contrib non-free" >> /etc/apt/sources.list
apt-get update
apt-get upgrade
mkdir /tmp/parallels
cd /tmp/parallels
cp -r /media/cdrom/* .
chmod -R 777 .
./install